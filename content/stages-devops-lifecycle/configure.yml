---
  title: Configure
  description: Here you can find information on how GitLab can help configure your applications and infrastructure. View more here!
  components:
    - name: sdl-cta
      data:
        title: Configure
        aos_animation: fade-down
        aos_duration: 500
        subtitle: Configure your applications and infrastructure.
        text: |
          GitLab helps teams to configure and manage their application environments. Strong integration to Kubernetes reduces the effort needed to define and configure the infrastructure required to support your application. Protect access to key infrastructure configuration details such as passwords and login information by using ‘secret variables’ to limit access to only authorized users and processes.
        icon:
          name: configure-alt-2
          alt: Configure Spotlight Icon
          variant: marketing
    - name: featured-media
      data:
        column_size: 6
        header: Product categories
        header_animation: fade-up
        header_animation_duration: 500
        media:
          - title: Auto DevOps
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Commit your code and GitLab does the rest to build, test, deploy, and monitor automatically. Eliminate the complexities of getting going with automated software delivery by automatically setting up the pipeline and necessary integrations, freeing up your teams to focus on the culture part.
            link:
              href: /stages-devops-lifecycle/auto-devops/
              text: Learn More
              data_ga_name: Auto DevOps
              data_ga_location: body
          - title: Kubernetes Management
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Connect Kubernetes clusters to GitLab for deployments and insights.
            link:
              href: /solutions/kubernetes/
              text: Learn More
              data_ga_name: Kubernetes Management
              data_ga_location: body
          - title: Deployment Management
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Enable platform engineers to use GitLab as their deployment platform: platform engineers can define common DevSecOps practices, streamline compliance, and share common patterns to enable application development teams to be more efficient.
            link:
              href: https://docs.gitlab.com/ee/topics/release_your_application.html
              text: Learn More
              data_ga_name: Deployment Management
              data_ga_location: body
          - title: ChatOps
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Tight integrations with Slack and Mattermost make it easy to manage and automate software development and delivery right from your chat app.
            link:
              href: https://docs.gitlab.com/ee/ci/chatops/
              text: Learn More
              data_ga_name: ChatOps
              data_ga_location: body
          - title: Infrastructure as Code
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Manage your infrastructure effectively to create, configure, and manage a complete software development environment.
            link:
              href: https://docs.gitlab.com/ee/user/infrastructure/iac/index.html
              text: Learn More
              data_ga_name: Infrastructure as Code
              data_ga_location: body
          - title: Cluster Cost Management
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Gain insights and recommendations about your cluster spending.
            link:
              href: https://docs.gitlab.com/ee/user/clusters/cost_management.html
              text: Learn More
              data_ga_name: Cluster Cost Management
              data_ga_location: body
    - name: copy
      data:
        aos_animation: fade-up
        aos_duration: 500
        block:
          - align_center: true
            text: |
              Learn more about our roadmap for upcoming features on our [Direction page](/direction/configure/){data-ga-name="configure direction" data-ga-location="body"}.
    - name: sdl-related-card
      data:
        column_size: 4
        header: Related
        cards:
          - title: Release
            icon:
              name: release-alt-2
              alt: Release Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 500
            text: |
              GitLab's integrated CD solution allows you to ship code with zero-touch, be it on one or one thousand servers.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/release/
              data_ga_name: Release
              data_ga_location: body
          - title: Verify
            icon:
              name: verify-alt-2
              alt: Verify Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 1000
            text: |
              Keep strict quality standards for production code with automatic testing and reporting.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/verify/
              data_ga_name: verify
              data_ga_location: body
          - title: Package
            icon:
              name: package-alt-2
              alt: Package Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 1500
            text: |
              Create a consistent and dependable software supply chain with built-in package management.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/package/
              data_ga_name: package
              data_ga_location: body
