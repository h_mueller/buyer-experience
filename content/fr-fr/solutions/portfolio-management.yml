---
  title: La gestion du portefeuille de projets
  description: La gestion du portefeuille de projets de GitLab vous permet de gérer des projets à grande échelle dans l'ensemble de votre organisation.
  components:
    - name: 'solutions-hero'
      data:
        title: La gestion du portefeuille de projets
        subtitle: Gérez des projets à grande échelle dans l'ensemble de votre organisation
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          text: Commencer votre essai gratuit
          url: /free-trial/
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Image: GitLab pour la gestion du portefeuille de projets"
    - name: 'solutions-feature-list'
      data:
        title: Suivre plusieurs programmes en cours tout en restant au fait de la performance du portefeuille de projets s'avère un véritable défi.
        subtitle: La gestion du portefeuille de projets de GitLab permet aux entreprises de planifier, de suivre et de mesurer la vélocité de plusieurs programmes pour qu'ils restent alignés sur les initiatives de l'entreprise.
        icon:
          name: /nuxt-images/logos/gitlab.svg
          alt: Icône GitLab
        features:
          - title: Gestion des programmes complexes
            description: |
              Les programmes complexes comportent des axes de travail différents, qui doivent être planifiés, suivis et mesurés afin de garantir le respect des échéances au sein du programme.

              * Avec les épopées [multi-niveaux](https://docs.gitlab.com/ee/user/group/epics/){data-ga-name="multi level epics" data-ga-location="body"} composées de plusieurs épopées enfants et leurs tickets associés, les entreprises peuvent maintenir la visibilité et le contrôle tout en privilégiant les initiatives qui génèrent le plus de valeur.
              * Organisez les épopées et les tickets par une simple action de glisser-déposer dans l'arbre des épopées afin de hiérarchiser le travail.
            icon:
              use_icon_component: true
              name: ci-cd
              alt: Icône CI-CD
              variant: marketing
              hex_color: '#9B51DF'
            image_url: /nuxt-images/solutions/portfolio-management/multi-level-epic.png
            image_alt: Épopées multi-niveaux dans GitLab
            image_tagline: |
              [Épopées multi-niveaux](https://docs.gitlab.com/ee/user/group/epics/){data-ga-name="epics" data-ga-location="body"}
          - title: Visualisation et mesure de la performance des programmes complexes
            description: |
              Élaborez la vision, la stratégie et la feuille de route du produit tout en profitant d'un insight sur l'avancement de votre programme interfonctionnel grâce à une vue de la [feuille de route à l'échelle du portefeuille de projets](https://docs.gitlab.com/ee/user/group/roadmap/){data-ga-name ="roadmap" data-ga-location = "body"}.

              * Faites un suivi précis de la [progression de chaque ticket et chaque épopée](https://docs.gitlab.com/ee/user/project/issues/index.html#health-status){data-ga-name="health status" data-ga-location="body"}, signalez les blocages et réagissez rapidement en consultant les indicateurs de progression.
              * Grâce aux [tableaux des épopées](https://docs.gitlab.com/ee/user/group/epics/epic_boards.html){data-ga-name="epic boards" data-ga-location="body"}, vous pouvez visualiser et suivre vos épopées ainsi que leurs flux de travail dans un tableau de style Kanban.
            icon:
              use_icon_component: true
              name: source-code
              alt: Icône de code source
              variant: marketing
              hex_color: '#52CDB7'
            image_url: /nuxt-images/solutions/portfolio-management/epic_view_roadmap.png
            image_alt: Feuille de route à l'échelle du portefeuille dans GitLab
            image_tagline: |
              [Feuille de route au niveau du portefeuille](https://docs.gitlab.com/ee/user/group/roadmap/){data-ga-name="portfolio level roadmap" data-ga-location="body"}
