---
  title: 公共部門のためのGitLab
  description: ソーシャルコーディング、継続的インテグレーション、リリースの自動化は、ミッションの目標を達成するために開発とソフトウェアの品質を加速することが証明されています。詳細を確認する。
  twitter_image: /nuxt-images/open-graph/gitlab-blog-cover.png
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        title: 公共部門向けGitLab
        subtitle: DevSecOpsプラットフォームは、ミッションへのスピードを加速します
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: 無料トライアルを開始する
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: ご不明な点がありますか？お問い合わせください
          url: /sales/
          data_ga_name: sales
          data_ga_location: hero
        image:
          image_url: /nuxt-images/solutions/public-sector.jpg
          image_url_mobile: /nuxt-images/solutions/public-sector.jpg
          alt: "画像: 公共部門向けGitLab"
          bordered: true
    - name: 'by-industry-intro'
      data:
        logos:
          - name: ワシントン大学のロゴ
            image: /nuxt-images/logos/uw-logo.svg
            url: https://about.gitlab.com/customers/uw/
            aria_label: ワシントン大学の顧客ケーススタディへのリンク
          - name: ロッキード・マーティンのロゴ
            image: /nuxt-images/logos/lockheed-martin.png
            url: https://about.gitlab.com/customers/lockheed-martin/
            aria_label: ロッキード・マーティンの顧客事例へのリンク
          - name: クック郡のロゴ
            image: /nuxt-images/logos/cookcounty-logo.svg
            url: https://about.gitlab.com/customers/cook-county/
            aria_label: クック郡の顧客ケーススタディへのリンク
          - name: サリー大学のロゴ
            image: /nuxt-images/case-study-logos/logo_uni_surrey.svg
            url: https://about.gitlab.com/customers/university-of-surrey/
            aria_label: サリー大学の顧客事例へのリンク
          - name: EABロゴ
            image: /nuxt-images/logos/eab-logo.svg
            url: https://about.gitlab.com/customers/EAB/
            aria_label: E.A.B.顧客事例へのリンク
          - name: ビクトリア大学ウェリントンのロゴ
            image: /nuxt-images/logos/victoria-university-wellington-logo.svg
            url: https://about.gitlab.com/customers/victoria_university/
            aria_label: ビクトリア大学ウェリントンの顧客事例へのリンク
    - name: 'side-navigation-variant'
      links:
        - title: 概要
          href: '#overview'
        - title: お客様の声
          href: '#testimonials'
        - title: 機能
          href: '#capabilities'
        - title: メリット
          href: '#benefits'
        - title: 顧客事例
          href: '#case-studies'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-benefits'
              data:
                title: セキュリティ。効率。コントロール。
                image:
                  image_url: "/nuxt-images/solutions/benefits/by-solution-benefits-public-sector.jpeg"
                  alt: コラボレーションイメージ
                is_accordion: true
                items:
                  - icon:
                      name: devsecops
                      alt: Devsecopsアイコン
                      variant: marketing
                    header: セキュリティとコンプライアンスのリスクを軽減
                    text: DevSecOpsライフサイクル全体にわたって一貫したガードレールを実施しながら、プロセスの早い段階でセキュリティとコンプライアンスの欠陥を発見します。
                    link_text: DevSecOpsの詳細はこちら
                    link_url: /solutions/security-compliance/
                    ga_name: セキュリティの削減についての詳細を確認
                    ga_location: benefits
                  - icon:
                      name: repo-code
                      alt: Repo Codeのアイコン
                      variant: marketing
                    header: リソースを解放する
                    text: コラボレーションとイノベーションを妨げる脆弱で複雑なDIYツールチェーンを排除することで、民間人の経験により良いサービスを提供し、グローバルな脅威を封じ込めます。
                    link_text: GitLabを選ぶ理由
                    link_url: /why-gitlab/
                    ga_name: free up learn more
                    ga_location: benefits
                  - icon:
                      name: digital-transformation
                      alt: デジタルトランスフォーメーションのアイコン
                      variant: marketing
                    header: 簡素化しながらモダン化
                    text: クラウドネイティブなアプリケーションと、それらが依存するインフラストラクチャの固有のニーズを満たすように設計された、DevSecOpsプラットフォームを使用します。
                    link_text: Uberのプラットフォームアプローチの詳細を確認
                    link_url: /solutions/devops-platform/
        - name: 'div'
          id: 'testimonials'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-quotes-carousel'
              data:
                header: |
                  政府から信頼され
                  <br />
                  開発者から愛される。
                quotes:
                  - title_img:
                      url: /nuxt-images/logos/usarmy.svg
                      alt: アメリカ陸軍
                    quote: 10種類のことを教える代わりに、1つのことを学んでもらうだけで、長い目で見ればはるかに負担が軽くなります。
                    author: Chris Apsey
                    position: アメリカ陸軍大尉
                    ga_carousel: アメリカ陸軍、公共部門
                  - title_img:
                      url: /nuxt-images/logos/uw-logo.svg
                      alt: ワシントン大学のロゴ
                    quote: この2年間で、GitLabはここワシントン大学の組織に変革をもたらしました。GitLabのプラットフォームは素晴らしいです!
                    author: Aaron Timss氏
                    position: CSE、情報ディレクター
                    ga_carousel: ワシントン公立大学、公共部門
                  - title_img:
                      url: /nuxt-images/logos/cookcounty-logo.svg
                      alt: クック郡のロゴ
                    quote: GitLabを使用すると、リサーチを作成したり、オフィスの人に見せることができます。研究の変更を提案された場合、バージョン管理を心配したり作業を保存したりすることなく、これらの変更を行うことができます。あるリポジトリのおかげで、実際の仕事にもっと集中できるようになり、仕事の仕組みはあまり気にならなくなりました。
                    author: Robert Ross
                    position: 最高データ責任者
                    ga_carousel: クック郡、公共部門
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-solutions-block'
              data:
                subtitle: 公共部門向けの最も包括的なDevSecOpsプラットフォーム
                sub_description: '安全で堅牢なソースコード管理(SCM)、継続的インテグレーション(CI)、継続的デリバリー(CD)、継続的なソフトウェアセキュリティとコンプライアンスを含むDevSecOpsプラットフォームから始まり、GitLabは次のような独自のニーズに対応します。'
                white_bg: true
                sub_image: /nuxt-images/solutions/public-sector/showcase-pubsec.svg
                alt: メリット画像
                solutions:
                  - title: SBOM
                    description: 既知の脆弱性など、使用されている依存関係の主要な詳細について、プロジェクトのソフトウェア部品表をレビューしましょう。
                    icon:
                      name: less-risk
                      alt: リスク低減アイコン
                      variant: marketing
                    link_text: 詳細はこちら
                    link_url: https://docs.gitlab.com/ee/user/application_security/dependency_list/
                    data_ga_name: sbom
                    data_ga_location: solutions block
                  - title: ゼロトラスト
                    description: GitLabがどのようにゼロトラストの原則を守り、ベストプラクティスを実証しているかをご覧ください。
                    icon:
                      name: monitor-pipeline
                      alt: モニターパイプラインのアイコン
                      variant: marketing
                    link_text: 詳細はこちら
                    link_url: /handbook/security/
                    data_ga_name: zero trust
                    data_ga_location: solutions block
                  - title: 脆弱性管理
                    description: パイプライン内、プロジェクトのグループ内、プロジェクトのグループ内、およびグループ間で、ソフトウェアの脆弱性をすべて1か所で管理できます。
                    icon:
                      name: shield-check
                      alt: シールドチェックのアイコン
                      variant: marketing
                    link_text: 詳細はこちら
                    link_url: https://docs.gitlab.com/ee/user/application_security/vulnerability_report/
                    data_ga_name: vulnerability management
                    data_ga_location: solutions block
                  - title: ファズテスト
                    description: GitLabを使用すると、包括的なスキャナセットと一緒に、パイプラインにファズテストを追加できます。ファズテストは、予期しない動作を引き起こすよう、アプリケーションに実装されたバージョンにランダムな入力を送信します。この動作は、対処すべきセキュリティとロジックの欠陥を示します。
                    icon:
                      name: monitor-test
                      alt: モニターテストアイコン
                      variant: marketing
                    link_text: 詳細はこちら
                    link_url: https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/
                    data_ga_name: fuzz testing
                    data_ga_location: solutions block
                  - title: オフライン環境
                    description: インターネットから切断された場合でも、ほとんどのGitLabセキュリティスキャナを実行できます。
                    icon:
                      name: gitlab-monitor-alt
                      alt: モニターGitLabアイコン
                      variant: marketing
                    link_text: 詳細はこちら
                    link_url: https://docs.gitlab.com/ee/user/application_security/offline_deployments/
                    data_ga_name: offline environment
                    data_ga_location: solutions block
                  - title: コンプライアンスの共通管理
                    description: 職務の分離、保護されたブランチ、プッシュルールなどの共通ポリシーを自動化して施行します。
                    icon:
                      name: shield-check
                      alt: シールドチェックのアイコン
                      variant: marketing
                    link_text: 詳細はこちら
                    link_url: https://docs.gitlab.com/ee/administration/compliance.html
                    data_ga_name: common controls for compliance
                    data_ga_location: solutions block
                  - title: コンプライアンスパイプライン
                    description: 必要なセキュリティスキャンが回避されないように、パイプラインスキャンの設定を強制します。
                    icon:
                      name: shield-check
                      alt: シールドチェックのアイコン
                      variant: marketing
                    link_text: 詳細はこちら
                    link_url: https://docs.gitlab.com/ee/administration/compliance.html
                    data_ga_name: compliance
                    data_ga_location: solutions block
                  - title: 低開発から高開発へ
                    description: さまざまな開発チーム間のコラボレーションを可能にします。
                    icon:
                      name: cog-code
                      alt: COGコードアイコン
                      variant: marketing
                    link_text: 詳細はこちら
                    link_url: https://www.youtube.com/watch?v=HSfDTslLRT8/
                    data_ga_name: low to high
                    data_ga_location: solutions block
                  - title: オンプレミス、セルフホスト、またはSaaS
                    description: GitLabはすべての環境で動作します。何を選ぶかはあなた次第です。
                    icon:
                      name: gitlab-monitor-alt
                      alt: モニターGitlabアイコン
                      variant: marketing
                    link_text: 詳細はこちら
                    link_url: /pricing/
                    data_ga_name: on-prem self-hosted or saas
                    data_ga_location: solutions block
                  - title: 強化されたコンテナイメージ
                    description: DoDに準拠して強化されたコンテナイメージは、リスク・プロファイルを最小化し、より安全なアプリケーションの迅速な展開を可能にし、運用プロセスの継続的な権限をサポートします。
                    icon:
                      name: lock-cog
                      alt: ロックとコグのアイコン
                      variant: marketing
                    link_text: 詳細はこちら
                    link_url: /press/releases/2020-07-01-gitlab-announces-hardened-container-image-in-support-of-the-us-department-of-defense-enterprise-devsecops-initiative.html
                    data_ga_name: hardened container image
                    data_ga_location: solutions block
        - name: 'div'
          id: 'benefits'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-value-prop'
              data:
                title: 公共部門に適したユニークなサービス
                cards:
                  - title: NIST SSDF
                    description: GitLabはNISTのガイダンスに沿っており、CIOがソフトウェアサプライチェーンセキュリティに必要なアクションを実装して、代理店を積極的に守るのに役立ちます。<a href="/blog/2022/03/29/comply-with-nist-secure-supply-chain-framework-with-gitlab/" data-ga-name="gitlab meets nist" data-ga-location="value prop">GitLab がどのように NIST SSDF 1.1 ガイダンスに準拠しているかについて詳しくは、こちらをご覧ください</a>
                    icon:
                      name: less-risk
                      alt: リスク低減のアイコン
                      variant: marketing
                  - title: DI2Eオルタナティブ
                    description: DI2E (国防情報エンタープライズ)へのアクセスがキャンセルされ、各機関はDevSecOpsモデル全体を再考することを余儀なくされました。GitLabはDI2Eの堅実な代替手段であり、当社の単一アプリケーションは調達を簡素化します。
                    icon:
                      name: devsecops
                      alt: DevSecOpsのアイコン
                      variant: marketing
                  - title: サプライチェーンの可視性と制御
                    description: GitLabのDevSecOpsプラットフォームは、エンドツーエンドの可視性とトレーサビリティを簡素化する単一の<a href="/press/releases/2020-07-01-gitlab-announces-hardened-container-image-in-support-of-the-us-department-of-defense-enterprise-devsecops-initiative.html" data-ga-name="hardened" data-ga-location="value prop">強化された</a>アプリケーションとして提供されます。セキュリティおよびコンプライアンスポリシーは、すべてのDevSecOpsプロセスで一貫して管理および施行されます。
                    icon:
                      name: eye-magnifying-glass
                      alt: 拡大鏡のアイコン
                      variant: marketing
                  - title: オンプレミス、セルフホスト、またはSaaS
                    description: 何を選ぶかはあなた次第です。
                    icon:
                      name: monitor-web-app
                      alt: Webアプリのモニターアイコン
                      variant: marketing
        - name: 'div'
          id: 'case-studies'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-case-studies'
              data:
                title: 顧客が実現したメリット
                link:
                  text: すべてのケーススタディ
                  link: /customers/
                rows:
                  - title: アメリカ陸軍サイバースクール
                    subtitle: 米陸軍サイバースクールがGitLabで「コードとしてのコースウェア」を作成した方法
                    image:
                      url: /nuxt-images/blogimages/us-army-cyber-school.jpeg
                      alt: ラジオで話す兵士
                    button:
                      href: /customers/us_army_cyber_school/
                      text: 詳しくはこちら
                      data_ga_name: us army learn more
                      data_ga_location: case studies
                  - title: クック郡の査定員事務所
                    subtitle: シカゴ州クック郡による透明性とバージョン管理を用いた経済データの評価方法
                    image:
                      url: /nuxt-images/blogimages/cookcounty.jpg
                      alt: 上空から見た家
                    button:
                      href: /customers/cook-county/
                      text: 詳細を見る
                      data_ga_name: cook country learn more
                      data_ga_location: case studies
                  - title: ワシントン大学
                    subtitle: Paul G. Allen Center for Computer Science & Engineeringは、10,000件を超えるプロジェクトを容易に管理できる制御性と柔軟性を確保しています。
                    image:
                      url: /nuxt-images/blogimages/uw-case-study-image.png
                      alt: ワシントン大学キャンパス
                    button:
                      href: /customers/uw/
                      text: 詳細を見る
                      data_ga_name: uw learn more
                      data_ga_location: case studies
    - name: 'by-solution-link'
      data:
        title: 'GitLabイベント'
        description: "イベントに参加して、チームがセキュリティとコンプライアンスを強化しながらも、ソフトウェアをより迅速かつ効率的に展開するための方法を学びましょう。2023年に当社が参加、または開催、運営予定のイベントは多数あります。みなさまのご参加を心よりお待ちしています!"
        link: /events/
        button_text: 詳しく見る
        image: /nuxt-images/events/google-cloud-next/pubsec-event-link.jpeg
        alt: プレゼンテーション中の聴衆
        icon: calendar-alt-2
