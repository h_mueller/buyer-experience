---
title: 無料でGitLabをお試しください
description: GitLab Ultimate 30日間の無料トライアルをお楽しみください。幅広い革新的な機能を備えた完全なソフトウェア開発ライフサイクルとDevOpsツールを体験してください。
components:
  - name: call-to-action
    data:
      title: GitLab Ultimateを30日間無料でお試し
      centered_by_default: true
      subtitle: 無料トライアルには、ほぼすべての[Ultimateランク](/pricing/ultimate/){data-ga-name="Free trial includes almost all Ultimate-tier" data-ga-location="header"}[[1]](#無料トライアルに含まれるもの){class="cta__subtitle--subscript"}機能が含まれています。クレジットカードは必要ありません[[2]](/pricing/#why-do-i-need-to-enter-credit-debit-card-details-for-free-pipeline-minutes){class="cta__subtitle--subscript" data-ga-name="credit card faq" data-ga-location="header"}
      body_text: 30日間のトライアルを今すぐ開始しましょう。 その後、GitLab Free を永久にご利用いただけます。
      aos_animation: fade-down
      aos_duration: 500
  - name: free-trial-plans
    data:
      saas:
        tooltip_text: |
          サービスとしてのソフトウェアは、ソフトウェアがサブスクリプションベースでライセンスされ、一元的にホストされるソフトウェアライセンスと配信モデルです。
        text: |
          準備はできています。 技術的なセットアップは必要ありません。 すぐに始めましょう。インストールは必要ありません。
        link:
          text: SaaSで続ける
          href: https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=free-trial
          data_ga_name: continue with saas
          data_ga_location: body
        bottom_text: |
          すでにアカウントをお持ちですか？[ログイン](https://gitlab.com/users/sign_in?redirect_to_referer=yes&__cf_chl_jschl_tk__=db2d336ba94805d0675008cc3fa3e0882d90953c-1619131501-0-AeQCSleOFTDGa9C-lXa3ZZZPpsO6sh0lCBCPZT0GxdT7tyOMAZoPzKppSQq9eV2Gqq9_kwKB8Lt8GJQ-nF-ra8updJRDfWTMBAwCR-m38kaHdAJYTicvW8Tj4KH55GO25zOeCYJexeEp1hx6f3DMvtjZd8elp_RfdulgN4-rxW8-lFSumJdSzE8y8N9FGltpsoQ8SKFSq41jMoB_GJ1nkIrjCU_kaGxJA3l4xhh-C14XFoBoBtfGjGOH4Kj76Y5QAeT7qemwuGBlvpYCK0OBv5aPkFDZ_Knp0W1zaOkr5tt511fra-rE3ekQI_lwR5VqBTHLtNslfgt4Il1SKLi6ZJLkces_WsUWdIQ3jNlyKbv08CF6kyDI3NiEOcCXUopCfQDYr-5syEUhv1Cnxy-Vjn7u5ejR2pvwIytWm8io2rhcaSOYxzxWccpxZLfjotTkzlrNP7KALbkxQOcNa_zeWVQ5t6aGC8H5wrT8u8ICxuJC){data-ga-name="log in" data-ga-location="body"}
      self_managed:
        form_id: 3438
        form_header: 準備が必要です。自分のインフラストラクチャまたはパブリッククラウド環境にGitLabをダウンロードしてインストールします。Linuxの経験が必要です。
  - name: faq
    data:
      aos_animation: fade-up
      aos_duration: 500
      header: GitLabトライアルのよくある質問（ FAQ ）
      groups:
        - header: 無料トライアルに含まれるもの
          data_toggle: What’s included in a free trial
          questions:
            - question: 無料トライアルには何が含まれていますか？ 何が含まれていませんか?
              answer: |
                無料トライアルには、[Ultimateランク](/pricing){data-ga-name="ultimate tier"data-ga-location="faq"}のほぼすべての機能が含まれていますが、例外は次のとおりです。
                *無料トライアルでは、どのレベルのサポートも含まれていません。 サポートの専門知識またはSLAパフォーマンスについてGitLabを評価したい場合は、オプションについて[sales](/sales/){data-ga-name="sales" data-ga-location="faq"}までお問い合わせください。
                * SaaS無料トライアルは、1か月あたり400個のコンピューティングユニットに制限されています。
                * SaaSでは、PremiumおよびUltimateのトライアルには、トライアルライセンスで利用可能な1つの[プロジェクトトークン](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html)が含まれます。
                * SaaSでは、無料トライアルには[グループ トークン](https://docs.gitlab.com/ee/user/group/settings/group_access_tokens.html)の使用は含まれていません。
            - question: 無料トライアルが終了したらどうなりますか？
              answer: |
                GitLab Ultimateの無料トライアルは30日間です。この期間が終わると、GitLab無料アカウントを永久に維持するか、[有料プラン](/pricing/){data-ga-name="paid plan" data-ga-location="faq"}にアップグレードすることができます。
        - header: SaaS vs.自己管理
          data_toggle: SaaS vs. Self-Managed
          questions:
            - question: SaaSと自己管理型のセットアップのちがいは何ですか?
              answer: |
                SaaS：準備はできています。技術的なセットアップは必要ないので、自分でGitLabをダウンロードしてインストールする必要はありません。 自己管理型：準備が必要です。自分のインフラストラクチャまたはパブリッククラウド環境にGitLabをダウンロードしてインストールします。Linuxの経験が必要です。
            - question: 特定の機能はSaaSまたは自己管理にのみ含まれていますか？
              answer: |
                一部の機能は、自己管理でのみ利用できます。 [機能の完全なリストはこちら](/pricing/feature-comparison/){data-ga-name="features list" data-ga-location="faq"}からご覧いただけます。
        - header: 価格と割引
          data_toggle: Pricing and discounts
          questions:
            - question: 無料トライアルにはクレジットカードやデビットカードが必要ですか？
              answer: |
                クレジット/デビットカードは、GitLab.com CI/CDを使用しない、自分のランナーを持参しない、または共有ランナーを無効にしているお客様には必要ありません。 ただし、GitLab.com共有ランナーを使用する場合は、クレジットカード/デビットカードの詳細が必要です。この変更は、GitLab.com で提供される無料のコンピューティング ユニットを暗号通貨マイニングに悪用することを防止するために行われました。カード情報を入力すると、1ドルの認証トランザクションで確認されます。料金はかかりませんし、送金も行われません。詳細については[こちら](/blog/2021/05/17/prevent-crypto-mining-abuse/){data-ga-name="prevent crypto mining abuse" data-ga-location="faq"}をご覧ください。
            - question: GitLabライセンスの価格はいくらですか？
              answer: |
                サブスクリプション情報は、[価格ページ](/pricing/){data-ga-name="pricing page" data-ga-location="faq"}に記載されています。
            - question: オープンソースプロジェクト、スタートアップ、または教育機関の特別価格を適用できますか？
              answer: |
                はい! 資格のあるオープンソースプロジェクト、スタートアップ、および教育機関には無料でUltimateライセンスを提供します。詳細については、[GitLab for Open Source](/solutions/open-source/){data-ga-name="gitlab for open source" data-ga-location="faq"}, [GitLab for Startups](/solutions/startups/){data-ga-name="gitlab for startups" data-ga-location="faq"}, and [GitLab for Education](/solutions/education/){data-ga-name="gitlab for education" data-ga-location="faq"}プログラム ページをご覧ください。
            - question: GitLab Freeから有料サブスクリプションにアップグレードするにはどうすればよいですか?
              answer: |
                GitLab Freeから有料ランクにアップグレードする場合は、[ドキュメント](https://docs.gitlab.com/ee/update/#community-to-enterprise-edition){data-ga-name="community to enterprise" data-ga-location="faq"}に従ってください。
        - header: インストールと移行
          data_toggle: Installation and migration
          questions:
            - question: 別のGitツールからGitLabに移行するにはどうすればよいですか?
              answer: |
                一般的なバージョン管理システムのプロジェクト移行手順については、[ドキュメント](https://docs.gitlab.com/ee/user/project/import/index.html){data-ga-name="migration" data-ga-location="faq"}を参照してください。
            - question: コンテナを使用してGitLabをインストールするにはどうすればよいですか?
              answer: |
                Dockerを使用したGitLabのインストールについては、[ドキュメント](https://docs.gitlab.com/omnibus/docker/README.html){data-ga-name="install docker" data-ga-location="faq"}を参照してください。
        - header: GitLabの統合
          data_toggle: GitLab integrations
          questions:
            - question: GitHostはまだ利用できますか？
              answer: |
                いいえ、GitHostの新規顧客の受け入れは終了しました。
            - question: GitLabはどのツールと統合されていますか？
              answer: |
                 GitLabは、いくつかのサードパーティとの統合を提供しています。 利用可能なサービスの詳細と統合方法については、[ドキュメント](https://docs.gitlab.com/ee/integration/README.html){data-ga-name="third party integration" data-ga-location="faq"}を参照してください。
