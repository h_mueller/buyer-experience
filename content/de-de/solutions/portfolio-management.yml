---
  title: Portfolio-Management
  description: Das Portfolio-Management von GitLab ermöglicht es dir, große unternehmensweite Projekte zu verwalten.
  components:
    - name: 'solutions-hero'
      data:
        title: Portfolio-Management
        subtitle: Verwalte große unternehmensweite Projekte
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          text: Kostenlose Testversion starten
          url: /free-trial/
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Bild: GitLab für das Portfolio-Management"
    - name: 'solutions-feature-list'
      data:
        title: Es ist eine Herausforderung, mehrere aktive Programme zu verfolgen und gleichzeitig die Portfolio-Performance stets im Blick zu haben.
        subtitle: Das Portfolio-Management von GitLab hilft Unternehmen, die Geschwindigkeit mehrerer Programme zu planen, zu verfolgen und zu messen, die an Geschäftsinitiativen ausgerichtet sind.
        icon:
          name: /nuxt-images/logos/gitlab.svg
          alt: GitLab-Symbol
        features:
          - title: Verwaltung großer Programme
            description: |
              Große Programme haben verschiedene Arbeitsabläufe, die geplant, verfolgt und gemessen werden müssen, um sicherzustellen, dass die Programme im Zeitplan liegen.

              * [Mehrstufige](https://docs.gitlab.com/ee/user/group/epics/){data-ga-name="multi level epics" data-ga-location="body"} Epics mit mehreren untergeordneten Epics und den damit verbundenen Tickets ermöglichen es Unternehmen, den Überblick und die Kontrolle zu behalten und gleichzeitig die Initiativen zu priorisieren, die den größten Wert bringen.
              * Organisiere Epics und Tickets per Drag & Drop innerhalb der Epic-Struktur, um die Arbeit zu priorisieren.
            icon:
              use_icon_component: true
              name: ci-cd
              alt: "Symbol: CI-CD"
              variant: marketing
              hex_color: '#9B51DF'
            image_url: /nuxt-images/solutions/portfolio-management/multi-level-epic.png
            image_alt: Mehrstufige Epics bei GitLab
            image_tagline: |
              [Mehrstufige Epics](https://docs.gitlab.com/ee/user/group/epics/){data-ga-name="epics" data-ga-location="body"}
          - title: Visualisierung und Messung der Performance großer Programme
            description: |
              Lege eine Produktvision, Strategie und Roadmap fest und erhalte einen Einblick in den Fortschritt deines funktionsübergreifenden Programms mit einer [Roadmap-Ansicht auf Portfolioebene](https://docs.gitlab.com/ee/user/group/roadmap/){data-ga-name="roadmap" data-ga-location="body"}.

              * Erkenne den [Integritätsstatus einzelner Tickets und Epics](https://docs.gitlab.com/ee/user/project/issues/index.html#health-status){data-ga-name="health status" data-ga-location="body"}, erstelle Berichte darüber und reagiere schnell darauf, indem du dir den Integritätszustand ansiehst.
              * Mit [Epic-Übersichten](https://docs.gitlab.com/ee/user/group/epics/epic_boards.html){data-ga-name="epic boards" data-ga-location="body"} kannst du deine Epics und ihre Workflows in einer Übersicht im Kanban-Stil visualisieren und verfolgen.
            icon:
              use_icon_component: true
              name: source-code
              alt: "Symbol: Quellcode"
              variant: marketing
              hex_color: '#52CDB7'
            image_url: /nuxt-images/solutions/portfolio-management/epic_view_roadmap.png
            image_alt: Roadmap auf Portfolioebene bei GitLab
            image_tagline: |
              [Roadmap auf Portfolioebene](https://docs.gitlab.com/ee/user/group/roadmap/){data-ga-name="portfolio level roadmap" data-ga-location="body"}
