---
  title: The DevSecOps Platform
  description: From planning to production, bring teams together in one application. Ship secure code more efficiently to deliver value faster.
  schema_org: >
    {"@context":"https://schema.org","@type": "Corporation","name":"GitLab","legalName":"GitLab Inc.","tickerSymbol":"GTLB","url":"https://about.gitlab.com","logo":"https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/logo/logo.png", "description" : "GitLab is The DevOps platform that empowers organizations to maximize the overall return on software development by delivering software faster and efficiently, while strengthening security and compliance. With GitLab, every team in your organization can collaboratively plan, build, secure, and deploy software to drive business outcomes faster with complete transparency, consistency and traceability.GitLab is an open core company which develops software for the software development lifecycle with 30 million estimated registered users and more than 1 million active license users, and has an active community of more than 2,500 contributors. GitLab openly shares more information than most companies and is public by default, meaning our projects, strategy, direction and metrics are discussed openly and can be found within our website. Our values are Collaboration, Results, Efficiency, Diversity, Inclusion & Belonging , Iteration, and Transparency (CREDIT) and these form our culture.","foundingDate":"2011","founders":[{"@type":"Person","name":"Sid Sijbrandij"},{"@type":"Person","name":"Dmitriy Zaporozhets"}],"slogan": "Our mission is to change all creative work from read-only to read-write so that everyone can contribute.","address":{"@type":"PostalAddress","streetAddress":"268 Bush Street #350","addressLocality":"San Francisco","addressRegion":"CA","postalCode":"94104","addressCountry":"USA"},"awards": "Comparably's Best Engineering Team 2021, 2021 Gartner Magic Quadrant for Application Security Testing - Challenger, DevOps Dozen award for the Best DevOps Solution Provider for 2019, 451 Firestarter Award from 451 Research","knowsAbout": [{"@type": "Thing","name": "DevOps"},{"@type": "Thing","name": "CI/CD"},{"@type": "Thing","name": "DevSecOps"},{"@type": "Thing","name": "GitOps"},{"@type": "Thing","name": "DevOps Platform"}],"sameAs":["https://www.facebook.com/gitlab","https://twitter.com/gitlab","https://www.linkedin.com/company/gitlab-com","https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg"]}

  cta_block:
    # Remove comments and add content to add pill badge to Homepage hero
      badge:
        text: "Meet GitLab Duo: AI-powered workflows →"
        link: "/gitlab-duo/"
        data_ga_name: "gitlab duo pill"
        data_ga_location: "hero"
      title: Software. Faster.
      subtitle: GitLab is the most comprehensive <br>AI-powered DevSecOps Platform.
      primary_button:
        text: "Get free trial"
        link: "https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial"
        data_ga_name: "free trial"
        data_ga_location: "hero"
      secondary_button:
        text: "What is GitLab?"
        modal:
          video_link: https://player.vimeo.com/video/799236905?h=4eee39a447&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479
          video_title: What is GitLab?
        data_ga_name: "watch demo"
        data_ga_location: "hero"
      image_copy: DevSecOps platform
      images:
        - id: 1
          image: "/nuxt-images/home/hero/developer-productivity-img.svg"
          alt: "Brand image of GitLab boards"
        - id: 2
          image: "/nuxt-images/home/hero/security-img.svg"
          alt: "Brand image of GitLab product"
        - id: 3
          image: "/nuxt-images/home/hero/value-stream-img.svg"
          alt: "Brand image of GitLab roadmap"

  customer_logos_block:
    text:
      legend: Trusted By
      url: "/customers/"
      data_ga_name: "trusted by"
      data_ga_location: "home case studies block"
    showcased_enterprises:
      - image_url: "/nuxt-images/home/logo_tmobile_mono.svg"
        link_label: Link to T-Mobile and GitLab webcast landing page"
        alt: "T-Mobile logo"
        url: https://learn.gitlab.com/c/learn-how-t-mobile-i?x=04KSqy
      - image_url: "/nuxt-images/home/logo_goldman_sachs_mono.svg"
        link_label: Link to Goldman Sachs customer case study
        alt: "Goldman Sachs logo"
        url: "/customers/goldman-sachs/"
      - image_url: "/nuxt-images/software-faster/airbus-logo.png"
        link_label: Link to Airbus customer case study
        alt: "Airbus logo"
        url: "/customers/airbus/"
      - image_url: "/nuxt-images/logos/lockheed-martin.png"
        link_label: Link to Lockheed Martin customer case study
        alt: "Lockheed martin logo"
        url: /customers/lockheed-martin/
      - image_url: "/nuxt-images/home/logo_nvidia_mono.svg"
        link_label: Link to Nvidia customer case study
        alt: "Nvidia logo"
        url: /customers/nvidia/
      - image_url: "/nuxt-images/home/logo_ubs_mono.svg"
        link_label: Link to blogpost How UBS created their own DevSecOps platform using GitLab
        alt: "UBS logo"
        url: /blog/2021/08/04/ubs-gitlab-devops-platform/
  featured_content:
    cards:
      - header: Get started with GitLab
        description: New to GitLab and not sure where to start? We’ll walk you through the basics so you know what to expect along the way.
        link:
          href: /get-started/
          text: Explore resources
          data_ga_name: get started
          data_ga_location: home content block
        icon: cog-check
      - header: "The Forrester Wave™: Integrated Software Delivery Platforms, Q2 2023"
        description: GitLab is the only leader in the report that compares 13 vendors over 26 criteria.
        link:
          href: "https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html"
          text: Read the report
          data_ga_name: 2023 forrester wave report
          data_ga_location: home content block
        icon: doc-pencil-alt
      - header: "GitLab's AI-assisted Code Suggestions"
        description: Reduce cognitive load and boost efficiency with the help of generative AI that suggests code as you type.
        link:
          href: "/solutions/code-suggestions/"
          text: Learn more
          data_ga_name: solutions code suggestions
          data_ga_location: home content block
        icon: ai-code-suggestions
  resource_card_block:
    column_size: 4
    header_text: Resources
    header_cta_text: View all resources
    header_cta_href: /resources/
    header_cta_ga_name: view all resources
    header_cta_ga_location: body
    cards:
      - icon:
          name: ebook-alt
          variant: marketing
          alt: Ebook Icon
        event_type: "Ebook"
        header: "Beginner's Guide to DevOps"
        link_text: "Read more"
        href: "https://page.gitlab.com/resources-ebook-beginners-guide-devops.html"
        image: "/nuxt-images/home/resources/Devops.png"
        alt: Winding path
        data_ga_name: "Beginner's Guide to DevOps"
        data_ga_location: "body"
      - icon:
          name: topics
          variant: marketing
          alt: Topics Icon
        event_type: "Topics"
        header: "What is CI/CD?"
        link_text: "Read more"
        href: "/topics/ci-cd/"
        image: "/nuxt-images/resources/fallback/img-fallback-cards-cicd.png"
        alt: Text reading CI/CD on a gradient
        data_ga_name: "What is CI/CD?"
        data_ga_location: "body"
      - icon:
          name: report-alt
          variant: marketing
          alt: Report Icon
        event_type: "Report"
        header: "2023 Global DevSecOps Report Series"
        link_text: "Read more"
        href: "/developer-survey/"
        image: "/nuxt-images/home/devsecops-survey-thumbnail.jpeg"
        alt: A globe outlined with the DevSecOps loop with the text What's next in DevSecOps
        data_ga_name: "2023 DevSecOps Survey"
        data_ga_location: "body"
      - icon:
          name: blog-alt
          variant: marketing
          alt: Blog Icon
        event_type: "Blog post"
        header: "AI/ML in DevSecOps Series"
        description: Follow our blog series detailing our integration of AI/ML throughout the software development lifecycle.
        link_text: "Read more"
        href: "https://about.gitlab.com/blog/2023/04/24/ai-ml-in-devsecops-series/"
        image: "/nuxt-images/home/resources/ai-experiment-stars.png"
        alt: ai-experiment-stars
        data_ga_name: "ai/ml in devsecops series"
        data_ga_location: "body"
      - icon:
          name: partners
          variant: marketing
          alt: Partners Icon
        event_type: "Partners"
        header: "Discover the benefits of GitLab on AWS"
        link_text: "Read more"
        href: "/partners/technology-partners/aws/"
        image: "/nuxt-images/home/resources/AWS.png"
        alt: "Text reading AWS on a gradient"
        data_ga_name: "Discover the benefits of GitLab on AWS"
        data_ga_location: "body"
      - icon:
          name: announce-release
          variant: marketing
          alt: Announce Release Icon
          hex_color: '#FFF'
        event_type: "Release"
        header: "GitLab 16.2 released with all new rich text editor experience"
        link_text: "Read more"
        href: "/releases/2023/07/22/gitlab-16-2-released/"
        alt: "GitLab version number on a gradient"
        image: "/nuxt-images/home/resources/16_2.png"
        data_ga_name: "GitLab 16.2 released with all new rich text editor experience"
        data_ga_location: "body"

  quotes_carousel_block:
    header: Teams do more with GitLab
    header_link:
      url: /customers/
      text: Read our case studies
      data_ga_name: Read our case studies
      data_ga_location: body
    quotes:
      - main_img:
          url: /nuxt-images/home/HohnAlan.jpg
          alt: Picture of Alan Hohn
        quote: "\u0022By switching to GitLab and automating deployment, teams have moved from monthly or weekly deliveries to daily or multiple daily deliveries.\u0022"
        author: Alan Hohn
        logo:
          url: /nuxt-images/logos/lockheed-martin.png
          alt: ''
        role: |
          Director of Software Strategy,
          Lockheed Martin
        statistic_samples:
          - data:
              highlight: 80x
              subtitle: faster CI pipeline builds
          - data:
              highlight: 90%
              subtitle: less time spent on system maintenance
        url: /customers/lockheed-martin/
        header: Lockheed Martin saves time, money, and tech muscle with GitLab
        data_ga_name: lockheed martin case study
        data_ga_location: home case studies
        cta_text: Read more
      - main_img:
          url: /nuxt-images/home/JasonManoharan.png
          alt: Picture of Jason Monoharan
        quote: "\u0022The vision that GitLab has in terms of tying strategy to scope and to code is very powerful. I appreciate the level of investment they are continuing to make in the platform.\u0022"
        author: Jason Monoharan
        logo:
          url: /nuxt-images/home/logo_iron_mountain_mono.svg
          alt: ''
        role: |
          VP of Technology,
          Iron Mountain
        statistic_samples:
          - data:
              highlight: $150k
              subtitle: approximate cost savings per year
          - data:
              highlight: 20 hours
              subtitle: saved in onboarding time per project
        url: customers/iron-mountain/
        header: Iron Mountain drives DevOps evolution with GitLab Ultimate
        data_ga_name: iron mountain
        data_ga_location: home case studies
        cta_text: Read More
      - main_img:
          url: /nuxt-images/home/EvanO_Connor.png
          alt: Picture of Evan O’Connor
        quote: "\u0022GitLab’s commitment to an open source community meant that we could engage directly with engineers to work through difficult technical problems.\u0022"
        author: Evan O’Connor
        logo:
          url: /nuxt-images/home/havenTech.png
          alt: ''
        role: |
          Platform Engineering Manager,
          Haven Technologies
        statistic_samples:
          - data:
              highlight: 62%
              subtitle: of monthly users ran secret detection jobs
          - data:
              highlight: 66%
              subtitle: of monthly users ran secure scanner jobs
        url: /customers/haven-technologies/
        header: Haven Technologies moved to Kubernetes with GitLab
        data_ga_name: haven technology
        data_ga_location: home case studies
        cta_text: Read more
      - main_img:
          url: /nuxt-images/home/RickCarey.png
          alt: Picture of Rick Carey
        quote: "\u0022We have an expression at UBS, ‘all developers wait at the same speed,’ so anything we can do to reduce their waiting time is value added. And GitLab allows us to have that integrated experience.\u0022"
        author: Rick Carey
        logo:
          url: /nuxt-images/home/logo_ubs_mono.svg
          alt: ''
        role: |
          Group Chief Technology Officer,
          UBS
        statistic_samples:
          - data:
              highlight: 1 million
              subtitle: successful builds in first six months
          - data:
              highlight: 12,000
              subtitle: active GitLab users
        url: /blog/2021/08/04/ubs-gitlab-devops-platform/
        header: UBS created their own DevOps platform using GitLab
        data_ga_name: ubs
        data_ga_location: home case studies
        cta_text: Read more
      - main_img:
          url: /nuxt-images/home/LakshmiVenkatrama.png
          alt: Picture of Lakshmi Venkatraman
        quote: "\u0022GitLab allows us to collaborate very well with team members and between different teams. As a project manager, being able to track a project or the workload of a team member helps prevent a project from delays. When the project is done, we can easily automate a packaging process and send results back to the customer. And with GitLab, it all resides within one house.\u0022"
        author: Lakshmi Venkatraman
        logo:
          url: /nuxt-images/home/singleron.svg
          alt: ''
        role: |
          Project Manager,
          Singleron Biotechnologies
        url: https://www.youtube.com/watch?v=22nmhrlL-FA
        header: Singleron uses GitLab to collaborate on a single platform to improve patient care
        data_ga_name: singleron video
        data_ga_location: home case studies
        cta_text: Watch the video

  solutions_block:
    subtitle: The way DevSecOps should be
    sub_image: /nuxt-images/home/solutions/solutions.png
    solutions:
      - title: Accelerate your digital transformation
        description: Reach your digital transformation objectives faster with a DevSecOps platform for your entire organization.
        icon:
          name: accelerate
          alt: Accelerate Icon
          variant: marketing
        link_text: Learn more
        link_url: /solutions/digital-transformation/
        data_ga_name: digital transformation
        data_ga_location: body
        image: /nuxt-images/home/solutions/accelerate.png
        alt: "Text bubbles of communicating teams"
      - title: Deliver software faster
        description: Automate your software delivery process so you can deliver value faster and quality code more often.
        icon:
          name: deliver
          alt: Deliver Icon
          variant: marketing
        link_text: Learn more
        link_url: /solutions/delivery-automation/
        data_ga_name: delivery automation
        data_ga_location: body
        image: /nuxt-images/home/solutions/deliver.png
        alt: "Text bubbles of communicating teams"
      - title: Ensure compliance
        description: Simplify continuous software compliance by defining, enforcing and reporting on compliance in one platform.
        icon:
          name: ensure
          alt: Ensure Icon
          variant: marketing
        link_text: Learn more
        link_url: /solutions/continuous-software-compliance/
        data_ga_name: continuous software compliance
        data_ga_location: body
        image: /nuxt-images/home/solutions/ensure.png
        alt: "Text bubbles of communicating teams"
      - title: Build in security
        description: Adopt DevSecOps practices with continuous software security assurance across every stage.
        icon:
          name: shield-check-light
          alt: Shield Check Icon
          variant: marketing
        link_text: Learn more
        link_url: /solutions/continuous-software-security-assurance/
        data_ga_name: continuous software security assurance
        data_ga_location: body
        image: /nuxt-images/home/solutions/build.png
        alt: "Text bubbles of communicating teams"
      - title: Improve collaboration and visibility
        description: Give everyone one platform to collaborate and see everything from planning to production.
        icon:
          name: improve
          alt: Improve Icon
          variant: marketing
        link_text: Learn more
        link_url: /solutions/devops-platform/
        data_ga_name: devops platform
        data_ga_location: body
        image: /nuxt-images/home/solutions/improve.png
        alt: "Text bubbles of communicating teams"
  badges:
    header: GitLab is The DevSecOps Platform
    tabs :
      - tab_name: Leaders in DevSecOps
        tab_id: leaders
        tab_icon:  /nuxt-images/icons/ribbon-check-transparent.svg
        copy: |
          **Our users have spoken.**
          GitLab ranks as a G2 Leader across DevSecOps categories
        cta:
          url: /analysts/
          cta_text: Learn more
          data_ga_name: analysts page
          data_ga_location: leaders in devops tab - badge section
        badge_images:
          - src: /nuxt-images/badges/enterpriseleader_fall2022.svg
            alt: G2 Enterprise Leader - Fall 2022
          - src: /nuxt-images/badges/midmarketleader_fall2022.svg
            alt: G2 Mid-Market Leader - Fall 2022
          - src: /nuxt-images/badges/smallbusinessleader_fall2022.svg
            alt: G2 Small Business Leader - Fall 2022
          - src: /nuxt-images/badges/bestresults_fall2022.svg
            alt: G2 Best Results - Fall 2022
          - src: /nuxt-images/badges/bestrelationshipenterprise_fall2022.svg
            alt: G2 Best Relationship Enterprise - Fall 2022
          - src: /nuxt-images/badges/bestrelationshipmidmarket_fall2022.svg
            alt: G2 Best Relationship Mid-Market - Fall 2022
          - src: /nuxt-images/badges/easiesttodobusinesswith_fall2022.svg
            alt: G2 Easiest To Do Business With Mid-Market - Fall 2022
          - src: /nuxt-images/badges/bestusability_fall2022.svg
            alt: G2 Best Usability - Fall 2022
      - tab_name: Industry Analyst Research
        tab_id: research
        tab_icon: /nuxt-images/icons/doc-pencil-transparent.svg
        copy: |
          **What Industry Analysts are saying about GitLab**
        cta:
          url: /analysts/
          cta_text: Learn more
          data_ga_name: analysts page
          data_ga_location: industry analyst research tab - badge section
        analysts:
          - logo: /nuxt-images/logos/forrester-logo.svg
            text: "GitLab is the only Leader in The Forrester Wave™: Integrated Software Delivery Platforms, Q2 2023"
            link:
              url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023
              data_ga_name: Forrester Integrated Software Delivery Platforms
          - logo: /nuxt-images/logos/gartner-logo.svg
            text: 'GitLab is a Leader in the 2023 Gartner® Magic Quadrant™ for DevOps Platforms'
            link:
              url: https://about.gitlab.com/gartner-magic-quadrant/
              data_ga_name: Gartner Magic Quadrant for DevOps Platform
          - logo: /nuxt-images/logos/gartner-logo.svg
            text: 'GitLab is a Challenger in the 2023 Gartner® Magic Quadrant™ for Application Security Testing'
            link:
              url: https://page.gitlab.com/resources-report-gartner-magic-quadrant-ast-2023.html
              data_ga_name: Gartner Magic Quadrant for Application Security Testing
  top-banner:
    text: The 2022 DevSecOps Survey is here, with insights from 5,000 DevOps pros.
    link:
      text: Explore the survey
      href: '/developer-survey/'
      ga_name: devsecops survey
      icon: gl-arrow-right

