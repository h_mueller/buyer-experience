---
  title: Chefkoch
  description: Learn how @chefkochde improved project visibility with GitLab.
  image_title: /nuxt-images/blogimages/chefkock_cover_image.jpg
  image_alt: 
  twitter_image: /nuxt-images/blogimages/chefkock_cover_image.jpg
  data:
    customer: Chefkoch
    customer_logo: /nuxt-images/customers/chefkoch_logo.svg
    heading: Chefkoch improved project visibility and deployment velocity with GitLab
    key_benefits:
      - label: Single source of truth
        icon: continuous-delivery
      - label: Improved workflow efficiency
        icon: accelerate
      - label: Less time spent on maintenance
        icon: cog-code
    header_image: /nuxt-images/blogimages/chefkock_cover_image.jpg
    customer_industry: Technology
    customer_employee_count: '33'
    customer_location: Bonn, Germany
    customer_solution: |
      [GitLab Self Managed Premium](/pricing/premium/){data-ga-name="premium solution" data-ga-location="customers hero"}
    sticky_benefits:
      - label: faster deployments
        stat: 40%
      - label: earlier identification of bugs
        stat: 30%
      - label: developer time saved
        stat: 20%
    blurb: Chefkoch, Europe's largest food platform, was looking for a way to streamline the company's developer workflows, improve project visibility, and provide controlled deployments.
    introduction: |
        With GitLab, the popular recipe platform, which offers customized recipes to millions of users a month, has been able to deploy faster, identify bugs sooner, and free up developers' valuable time — all while providing the end-to-end visibility the Chefkoch operations team needed.
    quotes:
      - text: |
          We have one tool with a global overview over the projects. We can easily see everything.
        author: Roman van Gemmeren
        author_role: Senior Systems Engineer
        author_company: Chefkoch
    content:
      - title: The largest food platform in Europe with a recipe for every occasion
        description: |
          [Chefkoch](https://www.chefkoch.de/){data-ga-name="chefkoch" data-ga-location="body"} was built on the belief that cooking and eating together makes us happier. Recipes can tell stories and pass down traditions. Chefkoch is now the largest food platform in Europe, with a mobile app boasting more than 350,000 recipes and a community of over 22 million users a month. Users can customize Chefkoch for their preferences, like vegetarian, paleo, or low carb, and find a tried-and-true recipe to match their tastes. Users can rate recipes, share them with friends, watch how-to videos, and be inspired to try something new. The right recipe creates shared moments of happiness, and Chefkoch wants to provide a user-centric experience so that their platform can be personalized and enjoyed by everyone — from beginners to pros.
      - title: Fragmented, manual toolchain
        description: |
          Chefkoch has a recipe ready for any occasion, but finding the right solutions to serve up their food platform experience proved to be much trickier. “We had no versioned deployments, no Git at all. There was a lot of manual work,” said Roman van Gemmeren, senior systems engineer at Chefkoch. “We had no real insight into how the infrastructure was working or performing.” Van Gemmeren leads the operations team that maintains the infrastructure and developer tools, and provides access to services like Kubernetes cluster management and monitoring.

          Chefkoch’s operations team was managing a complicated ecosystem with processes that were largely manual. Chefkoch was using Git with BitBucket before migrating to GitLab, but the Atlassian toolstack they had (Bitbucket, Bamboo) didn’t provide the metrics they needed for monitoring, and also didn’t integrate with Kubernetes. To properly support the development team and provide controlled deployments, Van Gemmeren’s team would need:

          - A streamlined developer workflow with better project visibility

          - Support for key integrations, like Kubernetes, Terraform, Docker, and JIRA
          
          - Autoscaling capabilities with less maintenance
          
          - Repository organization and CI/CD


          To start, Van Gemmeren knew that the team wanted Git repositories with better naming conventions. Even something like a repository path could alleviate a lot of the confusion in development. “We wanted more visibility and kind of a global administration possibility,” Van Gemmeren said. “Before, we had a repository name, XYZ, and Docker image, ABC, and there was no relation between those tools.” Van Gemmeren considered GitHub but greatly preferred something self-managed, rather than a public solution. For CI/CD, Van Gemmeren looked at Jenkins as a replacement for Bamboo, but decided that a plugin environment was going to be way too complex to properly maintain.
      - title: GitLab is the secret ingredient
        description: |
          GitLab’s DevOps platform with built-in repositories and CI/CD provided the end-to-end visibility the Chefkoch operations team had been looking for. GitLab also provided a seamless interface with simple access controls. “With Bamboo, we had to provide credentials for every project. Separate credentials, even. And with GitLab, we can just configure some clusters and provide the credentials at the group level,” said Van Gemmeren.

          For Van Gemmeren and his team, having visibility into what was happening in projects had been an ongoing issue. With so many tools spread out and not integrating with each other, it was difficult to know when issues were resolved. GitLab’s merge requests were able to remedy that problem by providing insight into discussions and logging actions through commits. The GitLab repositories and smooth pipelines made it easier for developers to know which Docker image belongs to which repository. The app development team experienced the greatest improvement to their processes — seeing almost 80% faster deployments. “The backend for frontend services can now be built and deployed in a very simple way by the app developers. That was not possible with Bamboo,” said Ingo Reinhart, lead developer (Android/iOS) at Chefkoch. Merge requests also improved code quality and allowed for better collaboration. “There are no more hurdles because of the CI interface. The app team is very satisfied with GitLab,” said Reinhart.

          GitLab’s integrations with Kubernetes, JIRA, and Grafana means that the operations team can now have oversight into projects that just weren’t possible before. “Right now, we have the metrics in GitLab for the project, specific ones, and more like a global overview in the Grafana stack that we maintain,” Van Gemmeren said. The operations team can reference specific GitLab commits and see when performance degraded or improved. It is worth mentioning that the Android app is no longer built by individual developers locally on the developer’s computer and is loaded manually into the Play Store. A developer can now do this automatically after a merge. We have thus increased compliance and reliability from zero to 80%.
      - title: A recipe for success
        description: |
          Chefkoch has the scalability they need with GitLab Runners and, without waiting for builds, Van Gemmeren estimates that the team is deploying to production 30% faster. GitLab’s Terraform integration means that deployments are managed from the CI/CD pipeline. GitLab has enabled them to adopt everything as code, from pipelines to infrastructure, and reduce the manual load.The streamlined developer workflow means that everyone, from dev to ops, has more visibility into projects. “Backend services, like an API gateway, can now be maintained and deployed by a mobile application developer,” said Van Gemmeren. “They’re all using the same tools and have the same UI experience.”

          Having GitLab as the single source of truth has been beneficial for the Chefkoch team because it also allows them to identify a single source of managing test failures in their application development. The new workflow has helped the team shift left and they are now detecting vulnerabilities and bugs 30% earlier in the software delivery lifecycle. Most tests were manual before they adopted GitLab, but now the operations team is using automated tests in their CI/CD pipelines in conjunction with Trivy for container scanning.

          One unexpected benefit of GitLab was how it improved the onboarding experience for new developers, trimming onboarding from two days to just two hours. With GitLab’s extensive documentation, new developers can adopt a more self-serve model and get up to speed quickly. Van Gemmeren estimates that developers have freed up about 20% of their time, now that they’re not having to focus so much on maintenance. Developers can now focus on developing code — not maintaining tools. “It’s a lot less time-consuming,” said Van Gemmeren.
    customer_success_stories:
      title: Customer success stories
      link:
        text: See all stories
        href: /customers/
        ga_name: all customers
        ga_location: customers stories
      read_story: Read story
      stories:
        - title: Goldman Sachs
          image: /nuxt-images/customers/goldman_sachs_case_study.png
          description: Goldman Sachs improves from 1 build every two weeks to over a thousand per day
          link: /customers/goldman-sachs/
          ga_name: Goldman Sachs
          ga_location: customers stories
        - title: Siemens
          image: /nuxt-images/customers/siemens.png
          description: How Siemens created an open source DevOps culture with GitLab
          link: /customers/siemens/
          ga_name: Siemens
          ga_location: customers stories
        - title: Fanatics
          image: /nuxt-images/customers/fanatics_case_study.png
          description: Fanatics' successful GitLab CI transition empowers innovation cycles and speed
          link: /customers/fanatics/
          ga_name: Fanatics
          ga_location: customers stories

