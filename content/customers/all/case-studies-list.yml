case_studies:

  - title: CARFAX
    description: CARFAX improves security, cuts pipeline management and costs with GitLab
    url: /customers/carfax/
    img_url: /nuxt-images/blogimages/carfax-banner.jpeg
    companySize: Mid-market (101 - 2000)
    industry:
      - Technology
    region: North America
    date_added: 2023-07-12

  - title: Deutsche Telekom
    description: Deutsche Telekom drives DevSecOps transformation with GitLab Ultimate
    url: /customers/deutsche-telekom/
    img_url: /nuxt-images/blogimages/deutsche-telekom-cover.jpg
    companySize: Enterprise (2001+)
    industry:
      - Technology
    region: North America
    date_added: 2023-05-19

  - title: Lockheed Martin
    description: Lockheed Martin saves time, money, and tech muscle with GitLab
    url: /customers/lockheed-martin/
    img_url: /nuxt-images/blogimages/lockheed-martin-cover-2.jpg
    companySize: Enterprise (2001+)
    industry:
      - Technology
    region: North America
    date_added: 2023-05-16

  - title: Iron Mountain
    description: Iron Mountain drives DevOps evolution with GitLab Ultimate
    url: /customers/iron-mountain/
    img_url: /nuxt-images/blogimages/iron-mountain-2.png
    companySize: Enterprise (2001+)
    industry:
      - Technology
    region: North America
    date_added: 2023-03-16

  - title: FullSave
    description: FullSave simplifies toolchain and increases deployments
    url: /customers/fullsave/
    img_url: /nuxt-images/blogimages/fullsave.jpeg
    companySize: Small to medium business (0 - 100)
    industry:
      - Technology
    region: Europe, the Middle East, and Africa
    date_added: 2023-01-13

  - title: Haven Technologies
    description: How Haven Technologies moved to Kubernetes with GitLab
    url: /customers/haven-technologies/
    img_url: /nuxt-images/blogimages/haven-technologies.jpg
    companySize: Mid-market (101 - 2000)
    industry:
      - Technology
    region: North America
    date_added: 2023-01-11

  - title: Deakin University
    description: Deakin University cuts toolchain sprawl with GitLab
    url: /customers/deakin-university/
    img_url: /nuxt-images/blogimages/deakin-university.jpg
    companySize: Enterprise (2001+)
    industry:
      - Education
    region: Asia-Pacific
    date_added: 2022-12-12

  - title: Dunelm Group PLC
    description: Dunelm shifts security left, boosts cloud move with GitLab
    url: /customers/dunelm/
    img_url: /nuxt-images/blogimages/dunelm.png
    companySize: Enterprise (2001+)
    industry:
      - Retail
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-23

  - title: HackerOne
    description: HackerOne achieves 5x faster deployments with GitLab’s integrated security
    url: /customers/hackerone/
    img_url: /nuxt-images/blogimages/hackerone-cover-photo.jpg
    companySize: Mid-market (101 - 2000)
    industry:
      - Technology
    region: North America
    date_added: 2022-03-23

  - title: The Zebra
    description: How The Zebra achieved secure pipelines in black and white
    url: /customers/thezebra/
    img_url: /nuxt-images/blogimages/thezebra_cover.jpg
    companySize: Mid-market (101 - 2000)
    industry:
      - Technology
    region: North America
    date_added: 2022-03-23

  - title: Drupal Association
    description: Drupal Association eases entry for new committers, speeds implementations
    url: /customers/drupalassociation/
    img_url: /nuxt-images/blogimages/drupalassoc_cover.jpg
    companySize: Mid-market (101 - 2000)
    industry:
      - Open source software
    region: North America
    date_added: 2022-03-22

  - title: Conversica
    description: Conversica leads AI innovation with help from GitLab Ultimate
    url: /customers/conversica/
    img_url: /nuxt-images/blogimages/conversicaimage.jpg
    companySize: Mid-market (101 - 2000)
    industry:
      - Technology
    region: North America
    date_added: 2022-03-22

  - title: U.S. Army Cyber School
    description: How the U.S. Army Cyber School created “Courseware as Code” with GitLab
    url: /customers/us_army_cyber_school/
    img_url: /nuxt-images/blogimages/us-army-cyber-school.jpeg
    companySize: Mid-market (101 - 2000)
    industry:
      - Public sector
    region: North America
    date_added: 2022-03-22

  - title: Siemens
    description: How Siemens created an open source DevOps culture with GitLab
    url: /customers/siemens/
    img_url: /nuxt-images/blogimages/siemenscoverimage_casestudy.jpg
    companySize: Enterprise (2001+)
    industry:
      - Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-22

  - title: Hilti
    description: How CI/CD and robust security scanning accelerated Hilti’s SDLC
    url: /customers/hilti/
    img_url: /nuxt-images/blogimages/hilti_cover_image.jpg
    companySize: Enterprise (2001+)
    industry:
      - Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-22

  - title: Moneyfarm
    description: Moneyfarm deploys faster using fewer tools with GitLab
    url: /customers/moneyfarm/
    img_url: /nuxt-images/blogimages/moneyfarm_cover_image_july.jpg
    companySize: Mid-market (101 - 2000)
    industry:
      - Financial services
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-22

  - title: Crédit Agricole Corporate & Investment Bank
    description: How Crédit Agricole Corporate & Investment Bank (CACIB) transformed its global workflow with GitLab
    url: /customers/credit-agricole/
    img_url: /nuxt-images/blogimages/creditagricole-cover-image.jpg
    companySize: Enterprise (2001+)
    industry:
      - Financial services
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-22

  - title: Keytrade Bank
    description: Keytrade Bank centralizes its tooling around GitLab
    url: /customers/keytradebank/
    img_url: /nuxt-images/blogimages/keytradebank_cover.jpg
    companySize: Enterprise (2001+)
    industry:
      - Financial services
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-22

  - title: Radio France
    description: Radio France deploys 5x faster with GitLab CI/CD
    url: /customers/radiofrance/
    img_url: /nuxt-images/blogimages/radio-france-cover-image.jpg
    companySize: Enterprise (2001+)
    industry:
      - Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-22

  - title: Kiwi.com
    description: How Kiwi.com transformed its workflow with GitLab and Docker
    url: /customers/kiwi/
    img_url: /nuxt-images/blogimages/kiwi-cover.jpg
    companySize: Enterprise (2001+)
    industry:
      - Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-22

  - title: Bendigo and Adelaide Bank
    description: Learn how GitLab is accelerating DevOps at Bendigo and Adelaide Bank
    url: /customers/bab/
    img_url: /nuxt-images/blogimages/bab_cover_image.jpg
    companySize: Enterprise (2001+)
    industry:
      - Financial services
    region: Asia-Pacific
    date_added: 2022-03-22

  - title: Fujitsu Cloud Technologies
    description: Fujitsu Cloud Technologies improves deployment velocity and cross-functional workflows with GitLab
    url: /customers/fujitsu/
    img_url: /nuxt-images/blogimages/fjct_cover.jpg
    companySize: Mid-market (101 - 2000)
    industry:
      - Technology
    region: Asia-Pacific
    date_added: 2022-03-22

  - title: EAB
    description: Increasing development speed to keep students in school
    url: /customers/EAB/
    img_url: /nuxt-images/blogimages/eab_case_study_Image.jpg
    companySize:  Mid-market (101 - 2000)
    industry:
      - Education
    region:  North America
    date_added: 2022-03-21

  - title: NVIDIA
    description: How GitLab Geo supports NVIDIA’s innovation
    url: /customers/nvidia/
    img_url: /nuxt-images/blogimages/nvidia.jpg
    companySize: Enterprise (2001+)
    industry:
      - Technology
    region: North America
    date_added: 2022-03-21

  - title: Zoopla
    description: How Zoopla deploys 700% faster with GitLab
    url: /customers/zoopla/
    img_url: /nuxt-images/blogimages/zoopla_cover_image.jpg
    companySize: Mid-market (101 - 2000)
    industry:
      - Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: University of Surrey
    description: The University of Surrey achieves top marks for collaboration and workflow management with GitLab
    url: /customers/university-of-surrey/
    img_url: /nuxt-images/solutions/education/university_of_surrey.jpg
    companySize: Enterprise (2001+)
    industry:
      - Education
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Airbus
    description: Airbus takes flight with GitLab releasing features 144x faster
    url: /customers/airbus/
    img_url: /nuxt-images/blogimages/airbus_cover_image.jpg
    companySize: Enterprise (2001+)
    industry:
      - Science & Research
    region: North America
    date_added: 2022-03-21

  - title: Glympse
    description: Glympse is making geo-location sharing easy
    url: /customers/glympse/
    img_url: /nuxt-images/blogimages/glympse_case_study.jpg
    companySize: Small to medium business (0 - 100)
    industry:
      - Technology
    region: North America
    date_added: 2022-03-21

  - title: ExtraHop
    description: ExtraHop fully embraces CI/CD process transformation with GitLab
    url: /customers/extra-hop-networks/
    img_url: /nuxt-images/blogimages/shahadat-rahman-gnyA8vd3Otc-unsplash.jpg
    companySize: Mid-market (101 - 2000)
    industry:
      - Technology
    region: North America
    date_added: 2022-03-21

  - title: Anchormen
    description: How GitLab CI/CD supports and accelerates innovation for Anchormen
    url: /customers/anchormen/
    img_url: /nuxt-images/blogimages/anchormen.jpg
    companySize: Small to medium business (0 - 100)
    industry:
      - Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: CERN
    description: Particle physics laboratory uses GitLab to connect researchers from across the globe
    url: /customers/cern/
    img_url: /nuxt-images/blogimages/cern.jpg
    companySize: Enterprise (2001+)
    industry:
      - Science & Research
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Goldman Sachs
    description: Goldman Sachs improves from 1 build every two weeks to over a thousand per day
    url: /customers/goldman-sachs
    img_url: /nuxt-images/blogimages/Goldman_Sachs_case_study.jpg
    companySize: Enterprise (2001+)
    industry:
      - Financial services
    region: North America
    date_added: 2022-03-21

  - title: Remote
    description: How Remote meets 100% of deadlines with GitLab
    url: /customers/remote
    img_url: /nuxt-images/blogimages/remote.jpg
    companySize: Small to medium business (0 - 100)
    industry:
      - Technology
    region: Remote
    date_added: 2022-03-21

  - title: Dublin City University
    description: How Dublin City University empowers students for the IT industry with GitLab
    url: /customers/dublin-city-university
    img_url: /nuxt-images/blogimages/dcu.jpg
    companySize: Mid-market (101 - 2000)
    industry:
      - Education
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Alteryx
    description: Removing multi-tool barriers to achieve 14 builds a day
    url: /customers/alteryx
    img_url: /nuxt-images/blogimages/alteryx_case_study.jpg
    companySize: Mid-market (101 - 2000)
    industry:
      - Technology
    region: North America
    date_added: 2022-03-21

  - title: Hotjar
    description: How Hotjar deploys 50% faster with GitLab
    url: /customers/hotjar
    img_url: /nuxt-images/blogimages/hotjar.jpg
    companySize: Small to medium business (0 - 100)
    industry:
      - Technology
    region: Remote
    date_added: 2022-03-21

  - title: Parimatch
    description: How Parimatch scores big with GitLab
    url: /customers/parimatch
    img_url: /nuxt-images/blogimages/pmbet_cover.jpg
    companySize: Mid-market (101 - 2000)
    industry:
      - Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Worldline
    description: Worldline improves code reviews’ potential by 120x
    url: /customers/worldline
    img_url: /nuxt-images/blogimages/worldline-case-study-image.jpg
    companySize: Enterprise (2001+)
    industry:
      - Financial services
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Nebulaworks
    description: How Nebulaworks replaced 3 tools with GitLab and empowered customer speed and agility
    url: /customers/nebulaworks
    img_url: /nuxt-images/blogimages/nebulaworks.jpg
    companySize: Small to medium business (0 - 100)
    industry:
      - Technology
    region: North America
    date_added: 2022-03-21

  - title: Cook County
    description: How Chicago’s Cook County assesses economic data with transparency and version control
    url: /customers/cook-county
    img_url: /nuxt-images/blogimages/cookcounty.jpg
    companySize: Mid-market (101 - 2000)
    industry:
      - Public sector
    region: North America
    date_added: 2022-03-21

  - title: University of Washington
    description: The Paul G. Allen Center for Computer Science & Engineering gains control and flexibility to easily manage 10,000+ projects.
    url: /customers/uw
    img_url: /nuxt-images/solutions/education/university_of_washington.jpg
    companySize: Mid-market (101 - 2000)
    industry:
      - Public sector
      - Education
    region: North America
    date_added: 2022-03-21

  - title: Trek10
    description: Trek10 provides radical visibility to clients
    url: /customers/trek10
    img_url: /nuxt-images/blogimages/trek10-bg.png
    companySize: Small to medium business (0 - 100)
    industry:
      - Technology
    region: North America
    date_added: 2022-03-21

  - title: Inventx AG
    description: How GitLab decreased deployment times from 2 days to just 5 minutes for Inventx AG
    url: /customers/inventx
    img_url: /nuxt-images/blogimages/cover_image_inventxag_case_study.jpg
    companySize: Mid-market (101 - 2000)
    industry:
      - Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: BGS
    description: How British Geological Survey revolutionized its software development lifecycle
    url: /customers/bgs
    img_url: /nuxt-images/blogimages/bgs-cover.jpg
    companySize: Mid-market (101 - 2000)
    industry:
      - Science & Research
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Weave
    description: GitLab delivers faster pipeline builds and improved code quality for Weave
    url: /customers/weave
    img_url: /nuxt-images/blogimages/weave_cover_image.jpg
    companySize: Small to medium business (0 - 100)
    industry:
      - Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Signicat
    description: Signicat reduces deploy time from days to just minutes
    url: /customers/signicat
    img_url: /nuxt-images/blogimages/signicat_cover_image.jpg
    companySize: Mid-market (101 - 2000)
    industry:
      - Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Paessler
    description: Paessler AG switched from Jenkins to GitLab and ramped up to 4x more releases
    url: /customers/paessler
    img_url: /nuxt-images/blogimages/paessler-case-study-image.png
    companySize: Mid-market (101 - 2000)
    industry:
      - Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Paessler PRTG
    description: How Paessler deploys up to 50 times daily with GitLab Premium
    url: /customers/paessler-prtg
    img_url: /nuxt-images/blogimages/paessler.jpg
    companySize: Mid-market (101 - 2000)
    industry:
      - Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: ANWB
    description: From Bicycles to Connected Driving
    url: /customers/anwb
    img_url: /nuxt-images/blogimages/anwb_case_study_image_2.jpg
    companySize: Enterprise (2001+)
    industry:
      - Retail
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Chorus
    description: The Future is Conversation Intelligence
    url: /customers/chorus/
    img_url: /nuxt-images/blogimages/Chorus_case_study.png
    companySize: Mid-market (101 - 2000)
    industry:
      - Technology
    region: North America
    date_added: 2022-03-21

  - title: Victoria University
    description: GitLab advances open science education at Te Herenga Waka – Victoria University of Wellington
    url: /customers/victoria_university
    img_url: /nuxt-images/solutions/education/victoria.jpeg
    companySize: Enterprise (2001+)
    industry:
      - Education
    region: Asia-Pacific
    date_added: 2022-03-21

  - title: BI Worldwide
    description: Increasing deployments to 10 times a day
    url: /customers/bi_worldwide
    img_url: /nuxt-images/blogimages/bi_worldwise_casestudy_image.png
    companySize: Enterprise (2001+)
    industry:
      - Consulting
    region: North America
    date_added: 2022-03-21

  - title: Lely
    description: How Lely replaced three tools with GitLab Self Managed Premium to maximize efficiency
    url: /customers/lely
    img_url: /nuxt-images/blogimages/lelyservicesoption2.jpg
    companySize: Mid-market (101 - 2000)
    industry:
      - Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Hemmersbach
    description: Hemmersbach reorganized their build chain and increased build speed 59x
    url: /customers/hemmersbach
    img_url: /nuxt-images/blogimages/hemmersbach_case_study.jpg
    companySize: Enterprise (2001+)
    industry:
      - Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Fanatics
    description: GitLab offers Fanatics the CI stability they were searching for
    url: /customers/fanatics
    img_url: /nuxt-images/blogimages/fanatics_case_study_image.png
    companySize: Mid-market (101 - 2000)
    industry:
      - Retail
    region: North America
    date_added: 2022-03-21

  - title: European Space Agency
    description: Connecting the cosmos with Earth - How the European Space Agency uses GitLab to focus on space missions
    url: /customers/european-space-agency
    img_url: /nuxt-images/blogimages/ESA_case_study_image.jpg
    companySize: Enterprise (2001+)
    industry:
      - Science & Research
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Curve
    description: Fintech innovator Curve counts on the GitLab platform
    url: /customers/curve
    img_url: /nuxt-images/blogimages/curve_cover_image.jpg
    companySize: Mid-market (101 - 2000)
    industry:
      - Europe, the Middle East, and Africa
    region: Financial services
    date_added: 2022-03-21

  - title: Potato
    description: How Potato uses GitLab CI for cutting edge innovation
    url: /customers/potato-london
    img_url: /nuxt-images/blogimages/potato-london.jpg
    companySize: Small to medium business (0 - 100)
    industry:
      - Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Sopra Steria
    description: How GitLab became the cornerstone of digital enablement for Sopra Steria
    url: /customers/sopra_steria
    img_url: /nuxt-images/blogimages/soprasteria.jpg
    companySize: Enterprise (2001+)
    industry:
      - Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Veepee
    description: How Veepee accelerated deployment from 4 days to 4 minutes
    url: /customers/veepee
    img_url: /nuxt-images/blogimages/veepee.jpg
    companySize: Enterprise (2001+)
    industry:
      - Retail
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: EveryMatrix
    description: Learn how EveryMatrix wins big with GitLab
    url: /customers/everymatrix
    img_url: /nuxt-images/blogimages/everymatrix_cover_image.jpg
    companySize: Mid-market (101 - 2000)
    industry:
      - Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Axway
    description: Axway realizes a 26x faster release cycle by switching from Subversion to GitLab
    url: /customers/axway
    img_url: /nuxt-images/blogimages/axway-case-study-image.png
    companySize: Enterprise (2001+)
    industry:
      - Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: SVA
    description: How SVA enhanced business agility workflow with GitLab
    url: /customers/sva
    img_url: /nuxt-images/blogimages/sva.jpg
    companySize: Mid-market (101 - 2000)
    industry:
      - Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: REGENHU
    description: How REGENHU reaps 50% YoY in cost savings annually with GitLab
    url: /customers/regenhu
    img_url: /nuxt-images/blogimages/cover_image_regenhu.jpg
    companySize: Small to medium business (0 - 100)
    industry:
      - Technology
      - Healthcare
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: New10
    description: How New10 deploys 3 times faster with GitLab
    url: /customers/new10
    img_url: /nuxt-images/blogimages/new-ten-euro.jpg
    companySize: Small to medium business (0 - 100)
    industry:
      - Financial services
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: KnowBe4
    description: Security provider KnowBe4 keeps code in-house and speeds up deployment
    url: /customers/knowbe4
    img_url: /nuxt-images/blogimages/knowbe4cover-sm.jpg
    companySize: Mid-market (101 - 2000)
    industry:
      - Technology
    region: North America
    date_added: 2022-03-21

  - title: Ibeo Automotive Systems
    description: GitLab places Ibeo Automotive Systems in pole position for driving innovation and scaling efficiencies
    url: /customers/ibeo_automotive
    img_url: /nuxt-images/blogimages/cover_image_ibeo_auto.jpg
    companySize: Mid-market (101 - 2000)
    industry:
      - Automotive
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Chefkoch
    description: Chefkoch improved project visibility and deploys 40% faster
    url: /customers/chefkoch
    img_url: /nuxt-images/blogimages/chefkock_cover_image.jpg
    companySize: Small to medium business (0 - 100)
    industry:
      - Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: Jasper Solutions
    description: How Jasper Solutions offers “DevSecOps in a box” with GitLab
    url: /customers/jasper-solutions
    img_url: /nuxt-images/blogimages/jasper.jpg
    companySize: Small to medium business (0 - 100)
    industry:
      - Technology
    region: North America
    date_added: 2022-03-21

  - title: SURF
    description: How SURF increased deployment speed by 1,400%
    url: /customers/surf
    img_url: /nuxt-images/blogimages/surf.jpg
    companySize: Mid-market (101 - 2000)
    industry:
      - Science & Research
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21

  - title: OW2
    description: Global nonprofit OW2 uses GitLab to help the developer community create software more efficiently
    url: /customers/ow2
    img_url: /nuxt-images/blogimages/ow2-case-study-image.png
    companySize: Small to medium business (0 - 100)
    industry:
      - Technology
    region: Europe, the Middle East, and Africa
    date_added: 2022-03-21
