---
  title: EveryMatrix
  description: Learn how EveryMatrix wins big with GitLab @EveryMatrix
  image_title: /nuxt-images/blogimages/everymatrix_cover_image.jpg
  image_alt: 
  twitter_image: /nuxt-images/blogimages/everymatrix_cover_image.jpg
  data:
    customer: EveryMatrix
    customer_logo: /nuxt-images/customers/everymatrix_logo.png
    heading: Learn how EveryMatrix wins big with GitLab
    key_benefits:
      - label: Seamless support for peer review
        icon: collaboration-alt-4
      - label: Improved automation
        icon: cogs
      - label: More transparency
        icon: open-source
    header_image: /nuxt-images/blogimages/everymatrix_cover_image.jpg
    customer_industry: Gaming
    customer_employee_count: '300'
    customer_location: Bucharest, Romania (HQ)
    customer_solution: |
      [GitLab Self Managed Premium](https://about.gitlab.com/pricing)
    sticky_benefits:
      - label: user growth in 1 year
        stat: 50%
      - label: yearly growth in pipelines
        stat: 42%
      - label: yearly growth in 1,700 projects
        stat: 25%
    blurb: When they decided to adopt a DevOps platform, EveryMatrix's development and infrastructure leaders turned to GitLab Self-Managed Premium.
    introduction: |
        The full-service B2B gaming platform provider wanted faster iterations and better control over open-source innovation.
    quotes:
      - text: |
          With GitLab, it’s not just a set of tools, it’s also the culture of the company. If there is an incident, we receive answers in a very transparent way. That’s important for our culture here, too. We are dealing with gaming players. If players feel you are not transparent, they run away. Trust is key.   
        author: Rafael Campuzano
        author_role: Group CTO
        author_company: EveryMatrix
    content:
      - title: Moving gaming services forward
        description: |
          [EveryMatrix](https://everymatrix.com/){data-ga-name="everymatrix" data-ga-location="body"} provides an API-based B2B product suite for casino, sports betting, payments and affiliate management in highly regulated industry segments across the globe. Elements provided include delivery of content, betting odds, and scores, as well as transactional support for real-time settlements, analytics, and risk management, all encompassed in a platform providing assured player protection. Work is supported by a dedicated infrastructure team that is responsible for governance and ensuring adherence to regional and local regulations. Services can be deployed as independent or integrated solutions. With live sports on hold during parts of the COVID-19 pandemic, the company successfully included more virtual sport events in its offerings. EveryMatrix solutions have evolved to cover more than 125 sports and virtual sports, comprising over 105,000 live events in early 2021.     
      - title: Building a practical open-source services delivery platform for a fast-paced sector
        description: |
         As a global provider of advanced gaming services, EveryMatrix is focused on fast-paced, innovative solution development and dependable software deployment for users in a wide selection of countries and regions. Such lively progress is tempered in gaming, as elsewhere, by a need for security, high availability, and reliability that helps ensure trust of clients and their online customers. With a growing focus on APIs and Kubernetes, it became clear the development teams needed more streamlined and automated workflows.

         General tooling advances and the need to install important add-ons underscored the requirement to adapt to tools flexibly and quickly. To effectively implement agile processes, EveryMatrix teams needed assurance that everything was in one place, that workflows were well-documented, and that front-line developers didn’t have to be versed in a different installation method for each tool they needed to use. Programmer teams needed dependable peer review and approvals processes. In addition, the system had to be capable of providing transparency to other stakeholders in the organization. CI/CD support that enabled such innovation was a must-have, and GitLab Premium was the choice.
       
      - title: A unified GitLab workflow and one DevOps platform
        description: |
          GitLab Premium implementation meets EveryMatrix requirements for a single repository that reduces the complexity of working with multiple open-source frameworks. Integrated, flexible tooling supports teams’ initiatives to automate repetitive tasks and to deploy open-source software quickly and securely. Now, developers no longer need to change from tool to tool, managing multiple different installations, according to Rafael Campuzano, Group CTO at EveryMatrix. GitLab thus supports the company’s daily DevOps-oriented project work by providing everything necessary in a single tool, with regularly updated functionality.

          All business units have implemented GitLab’s container registry, Campuzano said, and all teams are able to set up rules for code pushes through the repository. Importantly, he added, GitLab has been implemented in such a way that different business units can effectively interact with one another, while sharing and integrating code based on teams’ individual priority cycles. “GitLab for us is a really important tool to achieve an internal open-source model,” said Campuzano.
      - title: Trust is key for open-source model
        description: |
          The new platform has become key to improvements in processes and workflows, and now supports more than 200 developers. “With GitLab, all approval processes and code review mechanisms provide exactly the features we need. It’s a perfect fit with our intentions,” said Teodor Coman, Frontend Technical Director at EveryMatrix. “GitLab CI/CD tools are the core of our operations.”

          Teams can set up pipelines and manage deployments with much better control. Moreover, with GitLab, teams benefit from workflow approval and peer review capabilities. EveryMatrix team members give GitLab high marks for culture and adherence to open-source philosophy. Incidents and updates are handled in a transparent way. EveryMatrix has reaped huge benefits using GitLab; 50% user growth from 200 to 300 in just one year, 25% annual project growth, and over 42% annual growth in pipelines, with over 250,000 pipelines per year.

          “With GitLab, it’s not just a set of tools, it’s also the culture of the company. If there is an incident, we receive answers in a very transparent way. That’s important for our culture here, too,” said Campuzano. “We are dealing with gaming players. If players feel you are not transparent, they run away. Trust is key.”
    customer_success_stories:
      title: Customer success stories
      link:
        text: See all stories
        href: /customers/
      read_story: Read story
      stories:
        - title: Goldman Sachs
          image: /nuxt-images/customers/goldman_sachs_case_study.png
          description: Goldman Sachs improves from 1 build every two weeks to over a thousand per day
          link: /customers/goldman-sachs/
        - title: Siemens
          image: /nuxt-images/customers/siemens.png
          description: How Siemens created an open source DevOps culture with GitLab
          link: /customers/siemens
        - title: Fanatics
          image: /nuxt-images/customers/fanatics_case_study.png
          description: Fanatics' successful GitLab CI transition empowers innovation cycles and speed
          link: /customers/fanatics/

