---
  data:
    title: University of Washington
    description: 'The Paul G. Allen Center for Computer Science & Engineering gains control and flexibility to easily manage 10,000+ projects'
    og_title: University of Washington
    twitter_description: 'The Paul G. Allen Center for Computer Science & Engineering gains control and flexibility to easily manage 10,000+ projects.'
    og_description: 'The Paul G. Allen Center for Computer Science & Engineering gains control and flexibility to easily manage 10,000+ projects.'
    og_image: /nuxt-images/blogimages/uw-case-study-image.png
    twitter_image: /nuxt-images/blogimages/uw-case-study-image.png
    customer: University of Washington
    customer_logo: /nuxt-images/logos/uw-logo.svg
    customer_logo_full: true
    heading: 'The Paul G. Allen Center for Computer Science & Engineering gains control and flexibility to easily manage 10,000+ projects'
    key_benefits:
      - label: Increased scalability
        icon: auto-scale
      - label: Enhanced collaboration
        icon: collaboration-alt-4
      - label: Greater flexibility
        icon: arrows
    header_image: /nuxt-images/blogimages/uw-case-study-image.png
    customer_industry: Public sector - University
    customer_employee_count: 1,500 users
    customer_location: Seattle, Washington, USA
    customer_solution: |
      [GitLab Premium](/pricing/premium/){data-ga-name="premium solution" data-ga-location="customers hero"}
    sticky_benefits:
      - label: users suported by 1 engineer
        stat: 2000
      - label: projects hosted
        stat: 10,000
    blurb: The Paul G. Allen Center for Computer Science & Engineering (the Allen School) at the University of Washington (UW) is consistently ranked as one of the top computer science and computer engineering programs in the U.S.
    introduction: |
        Learn how GitLab has scaled to effectively host more than 10,000 projects, representing roughly 400 GB of data, in one instance
    quotes:
      - text: |
          Over the past two years, GitLab has been transformational for our organization here at UW. Your platform is fantastic!
        author: Aaron Timss
        author_role: Director of Information Technology
        author_company: CSE
    content:
      - title: The Paul G. Allen Center for Computer Science & Engineering (the Allen School) at the University of Washington (UW) is consistently ranked as one of the top computer science and computer engineering programs in the U.S.
        description: |
          The school is in the midst of a dramatic expansion as the demand for computer science degrees has never been higher. The Allen School currently consists of nearly 1,000 undergraduates, 250 PhD students, and 70 faculty members, who leverage version control and continuous integration tools to complete everything from class assignments to open source research and educational outreach projects. Aaron Timss, Director of Information Technology at the Allen School, leads a team of 20 software engineers and technical staff who are responsible for selecting and optimizing the tech stack that supports the school’s research, instructional, and development needs.
      - title: For years, the Allen School had been using Subversion (SVN) in conjunction with some homegrown scripts to support a flat, ad hoc version control system.
        description: |
          Faculty and students were getting frustrated with the slowness and workflow limitations of SVN, and having to rely on arcane Linux scripts to manage permissions on group directories and repositories. Fed up with SVN, they quietly turned to online Git repository managers, such as GitHub, to host their course assignments and collaborate on projects. But in some cases, students were inadvertently leaving their class assignments ‘public,’ violating university policy. The Allen School had world-facing open source projects. The critical requirement was to find a solution that provided federated login for external collaboration, while ensuring protections on sensitive projects, such as student coursework and unpublished research.

          Faced with these challenges, along with a rapidly growing user base, it was clear the Allen School IT team needed to find a solution that met both the collaboration and security requirements of their students and faculty. The team initially considered a number of self-managed options, including GitHub Enterprise. However, this platform didn’t provide an easy way for research teams to share and collaborate on their open source projects with external investigators or institutions. After some additional research, in the fall of 2014, the team decided to move forward with GitLab.
      - title: The Allen School has been using GitLab for more than two years and recently surpassed the milestone of their 10,000th project
        description: |
          Jason Howe, a software engineer on the school’s IT team, led the process of making GitLab available to the school’s students and faculty. Six months after rolling GitLab out, Howe authored provisioning tools on top of GitLab to enable faculty members to easily add students to course-specific projects. As more students started to use it as part of their class work, adoption climbed, and the number of projects on GitLab skyrocketed.

          IT staff noticed students had begun opting to host more of their personal development projects on the platform. And adoption by Allen School instructors jumped from the initial handful of early adopters to a few dozen. Other units within the university also began to be interested in using the platform. The IT team noted that it has been extremely pleased with GitLab’s ability to easily scale with the demands of their growing organization, and they appreciate GitLab’s fervor for further developing the platform and staying on top of security issues. Even as usage of GitLab continues to grow, Howe and the team still find the product easy to maintain. They spend just one to two days per quarter updating and maintaining the platform.

          The Allen School’s students, faculty, and IT team are happy with their decision to choose GitLab. Howe sums up GitLab’s benefits in two words: control and flexibility. From an admin or systems perspective, GitLab gives the IT team the necessary controls to ensure that sensitive university research and students’ coursework are all easily manageable and kept safe. And in terms of flexibility, GitLab is open source, which makes it possible for the IT team to build unique SSO and provisioning tools against GitLab’s API.
