/label ~"dex-approval::2-standard"


## Step 1: What is changing in this MR?

Please describe the change and link any relevant issues.

## Step 2: Ensure that your changes comply with the following, where applicable:

- [ ] I, the Assignee, have run [Axe tools](https://chrome.google.com/webstore/detail/axe-devtools-web-accessib/lhdoppojpmngadmnindnejefpokejbdd) on any updated pages, and fixed the relevant accessibility issues.
- [ ] These changes meet a specific OKR or item in our Quarterly Plan.
- [ ] These changes work on both Safari, Chrome, and Firefox.
- [ ] These changes have been reviewed for Visual Quality Assurance and Functional Quality Assurance on Mobile, Desktop, and Tablet.
- [ ] These changes work with our Google Analytics and SEO tools.
- [ ] These changes have been documented as expected.

## Step 3: Add the appropriate labels for triage

This MR will have `dex-approval::2-standard` automatically applied, but please update it as follows. If deciding between two levels, go with the higher of the two: https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/sites/handbook/source/handbook/marketing/digital-experience/marketing-site-approval-process/index.html.md

## Step 4: Tag the appropriate person for review

Depending on which label is used, you may tag the following people as a `Reviewer` on this MR: https://about.gitlab.com/handbook/marketing/digital-experience/marketing-site-approval-process/#step-3-tag-the-appropriate-people-for-review
